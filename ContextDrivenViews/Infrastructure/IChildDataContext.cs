namespace ContextDrivenViews.Infrastructure
{
    public interface IChildDataContext
    {
        string Title { get; }
    }
}