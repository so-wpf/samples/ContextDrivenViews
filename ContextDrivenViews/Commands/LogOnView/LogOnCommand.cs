namespace ContextDrivenViews.Commands.LogOnView
{
    using System;
    using System.ComponentModel;
    using System.Windows.Input;
    using ViewModels.Singletons;

    public sealed class LogOnCommand : ICommand, IDisposable
    {
        readonly object _disposeLock = new object();
        readonly LogOnViewModel _vm;
        bool _isDisposed;

        internal LogOnCommand(LogOnViewModel vm)
        {
            _vm = vm;

            PropertyChangedEventManager.AddHandler(_vm, RaiseCanExecuteChanged, nameof(LogOnViewModel.EmailAddress));
        }

        public bool CanExecute(object parameter)
        {
            return !string.IsNullOrEmpty(_vm.EmailAddress) &&
                   _vm.EmailAddress.Length > 3 && _vm.EmailAddress.Contains("@");
        }

        public void Execute(object parameter)
        {
            _vm.RaiseLogOnSuccessful();
        }

        public event EventHandler CanExecuteChanged;

        public void Dispose()
        {
            lock (_disposeLock)
            {
                if (_isDisposed) return;

                PropertyChangedEventManager.RemoveHandler(_vm, RaiseCanExecuteChanged,
                    nameof(LogOnViewModel.EmailAddress));

                _isDisposed = true;
            }
        }

        void RaiseCanExecuteChanged(object sender,
            PropertyChangedEventArgs propertyChangedEventArgs)
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}