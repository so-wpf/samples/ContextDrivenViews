﻿namespace ContextDrivenViews.ViewModels.Singletons
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using Infrastructure;
    using JetBrains.Annotations;

    public sealed class AuthorizedViewModel : INotifyPropertyChanged, IChildDataContext, IDisposable
    {
        const string GravatarBaseUrl = "https://www.gravatar.com/avatar/{0}?d=identicon&s=48";
        readonly object _disposeLock = new object();
        string _emailAddress;
        Uri _gravatarUrl;
        bool _isDisposed;

        AuthorizedViewModel()
        {
            Title = "Welcome!";

            LogOnViewModel.Instance.LogOnSuccessful += HandleSuccessfulLogOn;
        }

        public static AuthorizedViewModel Instance { get; } = new AuthorizedViewModel();

        public string EmailAddress
        {
            get { return _emailAddress; }

            private set
            {
                if (value == _emailAddress) return;
                _emailAddress = value;
                NotifyPropertyChanged(nameof(EmailAddress));
            }
        }

        [SuppressMessage("Microsoft.Naming",
            "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gravatar")]
        public Uri GravatarUrl
        {
            get { return _gravatarUrl; }

            private set
            {
                if (value == _gravatarUrl) return;
                _gravatarUrl = value;
                NotifyPropertyChanged(nameof(GravatarUrl));
            }
        }

        public string Title { get; }

        public void Dispose()
        {
            lock (_disposeLock)
            {
                if (_isDisposed) return;

                LogOnViewModel.Instance.LogOnSuccessful -= HandleSuccessfulLogOn;

                _isDisposed = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [SuppressMessage("Microsoft.Globalization",
            "CA1308:NormalizeStringsToUppercase", Justification = "Gravatar requires lower casing")]
        void HandleSuccessfulLogOn(object sender, EventArgs e)
        {
            EmailAddress = (sender as LogOnViewModel)?.EmailAddress ?? string.Empty;

            using (var md5 = new MD5Cng())
            using (var reader = new MemoryStream(Encoding.UTF8.GetBytes(EmailAddress.Trim().ToLowerInvariant())))
            {
                GravatarUrl = new Uri(string.Format(CultureInfo.InvariantCulture, GravatarBaseUrl,
                    BitConverter.ToString(md5.ComputeHash(reader)).Replace("-", string.Empty).ToLowerInvariant()));
            }
        }

        [NotifyPropertyChangedInvocator]
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyName));
        }
    }
}