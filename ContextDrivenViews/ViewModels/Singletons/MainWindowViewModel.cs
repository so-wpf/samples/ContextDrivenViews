﻿namespace ContextDrivenViews.ViewModels.Singletons
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Infrastructure;
    using JetBrains.Annotations;

    public sealed class MainWindowViewModel : INotifyPropertyChanged, IDisposable
    {
        readonly List<IDisposable> _disposables = new List<IDisposable>();
        readonly object _disposeLock = new object();
        readonly string _title;
        IChildDataContext _childDataContext;

        bool _isDisposed;

        MainWindowViewModel()
        {
            _title = "Content Driven Views";

            var logOnViewModel = LogOnViewModel.Instance;
            logOnViewModel.LogOnSuccessful += HandleSuccessfulLogOn;

            ChildDataContext = logOnViewModel;

            _disposables.Add(logOnViewModel);
            _disposables.Add(AuthorizedViewModel.Instance);
        }

        public static MainWindowViewModel Instance { get; } = new MainWindowViewModel();

        public string Title => ChildDataContext?.Title == null
            ? _title
            : string.Concat(_title, " - ", ChildDataContext?.Title);

        public IChildDataContext ChildDataContext
        {
            get { return _childDataContext; }

            private set
            {
                if (Equals(value, _childDataContext)) return;

                _childDataContext = value;

                NotifyPropertyChanged(nameof(ChildDataContext));
                NotifyPropertyChanged(nameof(Title));
            }
        }

        //simplification of dispose pattern as we don't have unmanaged resources
        public void Dispose()
        {
            lock (_disposeLock)
            {
                if (_isDisposed) return;

                foreach (var disposable in _disposables)
                    disposable.Dispose();

                _isDisposed = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void HandleSuccessfulLogOn(object sender, EventArgs e)
        {
            ChildDataContext = AuthorizedViewModel.Instance;
            LogOnViewModel.Instance.LogOnSuccessful -= HandleSuccessfulLogOn;
        }

        [NotifyPropertyChangedInvocator]
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyName));
        }
    }
}