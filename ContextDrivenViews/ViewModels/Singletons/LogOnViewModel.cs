namespace ContextDrivenViews.ViewModels.Singletons
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using Commands.LogOnView;
    using Infrastructure;
    using JetBrains.Annotations;

    public sealed class LogOnViewModel : INotifyPropertyChanged, IChildDataContext, IDisposable
    {
        readonly object _disposeLock = new object();
        string _emailAddress;
        bool _isDisposed;

        LogOnViewModel()
        {
            Title = "LogOn";
            LogOnCommand = new LogOnCommand(this);
        }

        public static LogOnViewModel Instance { get; } = new LogOnViewModel();

        public LogOnCommand LogOnCommand { get; }

        public string EmailAddress
        {
            get { return _emailAddress; }

            set
            {
                if (value == _emailAddress) return;
                _emailAddress = value;
                NotifyPropertyChanged(nameof(EmailAddress));
            }
        }

        public string Title { get; }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed",
            MessageId = "<LogOnCommand>k__BackingField", Justification = "It is being called via property")]
        public void Dispose()
        {
            lock (_disposeLock)
            {
                if (_isDisposed) return;

                LogOnCommand.Dispose();

                _isDisposed = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyName));
        }

        public event EventHandler LogOnSuccessful;

        // normally this would be internal with InternalsVisibleTo configured via AssemblyInfo.cs
        // allowing the logOn command to call this and signal success but leaving it as public
        // for the simplified sample use
        [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")]
        public void RaiseLogOnSuccessful()
        {
            LogOnSuccessful?.Invoke(this, EventArgs.Empty);
        }
    }
}