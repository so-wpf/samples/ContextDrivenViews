﻿namespace ContextDrivenViews
{
    using System;
    using ViewModels.Singletons;

    public partial class MainWindow
    {
        public MainWindow()
        {
            Closed += OnClosed;
            InitializeComponent();
        }

        void OnClosed(object sender, EventArgs eventArgs)
        {
            (DataContext as MainWindowViewModel)?.Dispose();

            Closed -= OnClosed;
        }
    }
}